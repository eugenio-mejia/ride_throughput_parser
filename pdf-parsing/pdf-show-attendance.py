import os
import re

import dateutil
import pandas as pd


def load_file_paths(directory, inc_ext=['pdf']):
    """Gets file names of file types from a directory"""

    file_names = [
                  os.path.join(directory, file_name) \
                  for file_name in os.listdir(os.path.realpath(directory)) \
                  if any(file_name.endswith(ext) for ext in inc_ext)
                  ]

    return file_names


def dict_update(original, new_dict):
    #TODO: figure out why DICT.update({}) was rendering None
    for k, v in new_dict.items():
        original[k]=v

    return original


import functools
def debug(f):
    """A decorator to debug"""

    @functools.wraps(f)
    def wrapper():
        print(__name__)
        return f

    return wrapper


def sweep_documents(directory):
    """Looks through the documents of a given document"""

    entries = []

    file_names = load_file_paths(directory, inc_ext=['txt'])
    for file_name in file_names:
        print()
        print('--------------------------------------------')
        print('    Loading ' + file_name)
        print('--------------------------------------------')
        # TODO: 
        # wb = load_workbook(file_name = os.path.join(directory, file_name))

        # TODO: convert pdftotext
        # os.system('pdftotext -format ' + pdf_file_name)

        with open(file_name, 'r') as text_file:
            lines = text_file.readlines()
            first_time = True
            for i in range(0, len(lines)):
                if line_is_time(lines[i]):
                    if first_time:
                        first_time = False
                        print(i, type(lines[i]), lines[i])
                        title = analyze_show_title(lines, i)
                        # subtitle = analyze_show_subtitle(lines, i)
                        # date = analyze_date(lines, i)
                    # time = analyze_time(lines, i)
                    # row = {'title':title, 'subtitle':subtitle, 'date':date}
                    # print(row)

                    # # TODO: replace with DICT.update()
                    # row = dict_update(row, time)
                    # print(row)

                    # entries.append(row)
                else:
                    first_time = True
    return entries


def save(df, file_name):
    """Write DataFrame to a file"""
    df.to_excel(file_name)


def run():
    main()


def main():
    """
    Description:
        - read lines one by one
        - if line contains AM/PM, flag
        - line[-1] contains the date
        - line[-2] contains subtitle
        - line[-3] contains show_name (possibly avg_attendance_per_day, number_of_shows and avg_attendance_per_show)
        - line[0] contains time, show_capacity and show_attendance
        - if next line contains AM/PM, gather time, show_capacity and show_attendance
        - if next line does not contain AM/PM, stop extracting and go back to initial step
    """

    # sweep_documents()
    # sweep_documents('attraction-operational-readiness-reports')
    # sweep_documents('test')
    rows = sweep_documents('shows')
    wrap(rows)

    
def wrap(rows):
    # ROWS is modified by sweep_documents before this step
    df = pd.DataFrame(rows)

    column_names = ['title',
                    'subtitle',
                    'date',
                    'time',
                    'capacity',
                    'attendance'
                    ]

    print(df)

    # df = df[column_names]
    # sorted_df = df.sort_values(by=['title','date','time'])
    # save(sorted_df, 'output.xlsx')


def line_is_time(line):
    """Checks if the current line has a date, which is the flag to do some more analysis on the lines around it"""

    pattern = r'\d{2}\:\d{2}\s[A/P]M'
    matches = re.findall(pattern, line)

    if len(matches) > 0:
        return True

    return False


def analyze_time(lines, index, offset=0):
    line = lines[index-offset]
    # TODO: check time regex
    pattern = r'(\d{2}\:\d{2}\s[A/P]M)'
    # pattern = r'(\d{2}:\d{2}\b[A,P]M)([\d,])+\b([\d,]+)'
    # regex example line
    #                                           12:00 PM         500         375

    match = re.search(pattern, line)
    time = dateutil.parser.parse(match.group(0))

    # remove commas
    capacity = re.sub('[,]', '', match.group(1))
    attendance = re.sub('[,]', '', match.group(2))

    return {
            'time' : time,
            'capacity' : capacity,
            'attendance' : attendance,
            }


def analyze_date(lines, index, offset=1):
    line = lines[index-offset]
    #TODO: check date regex
    pattern = r'\b([a-zA-Z]{3}\b\d{2}\/\d{2}\/\d{4})'
    # regex example line
    #                    Fri 04/14/2017                        1,500       1,260                                                                 3             420       16,720
    match = re.search(pattern, line)
    date = dateutil.parser.parse(match.group(0))

    return {
            'date' : date
            }


def analyze_show_subtitle(lines, index, offset=2):
    # regex example line
    #            Live at The Back Porch                        1,500       1,260                                        1             1,260      3             420       16,720
    data = analyze_show_title(lines, index, offset=offset)
    return data
    #TODO: change data key 'show_title' to 'show_subtitle'


def analyze_show_title(lines, index, offset=3):
    line = lines[index-offset]
    #TODO: check title regex
    # pattern = r'\b([a-zA-Z\b]+)\b{2+}([\d,]+)\b+([\d,]+)'
    pattern = r'\b([a-zA-Z\b]+)\s{2+}([\d,]+)\b+([\d,]+)'
    # regex example line
    #      Backporch Theatre                                   1,500       1,260                                        1             1,260      3             420       16,720

    match = re.search(pattern, line)

    return {
            'show_title' : match.group(0),
            # 'show_capacity' : match.group(1),
            # 'show_attendance' : match.group(2),
            }


if __name__ == '__main__':
    main()
